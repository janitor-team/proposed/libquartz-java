#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
SVN=http://svn.terracotta.org/svn/quartz/tags
VERSION=$2
TAR=../libquartz-java_$VERSION.orig.tar.xz
DIR=libquartz-java-$VERSION.orig

# clean up the upstream tarball
svn export $SVN/quartz-$VERSION $DIR
XZ_OPT=--best tar -c -J -v -f $TAR -X debian/orig-tar.exclude $DIR
rm -rf $3 $DIR
